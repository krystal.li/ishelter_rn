
import { Dimensions, StatusBar, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: StatusBar.currentHeight || 0,
    },
    item: {
      width: Dimensions.get('window').width / 2.5,
      height: Dimensions.get('window').width / 2,
      alignItems: 'center',
      marginHorizontal: 20,
      borderRadius: 8,
    },
    favorite_container: {
      position: 'absolute',
      right: 36,
      top: 12,
      height: 16,
      width: 10,
      justifyContent: 'center',
      alignItems: 'center',
    },
    favorite: {
      resizeMode: 'cover',
      height: 32,
      width: 32,
    },
    title: {
      width: Dimensions.get('window').width / 2.5,
      paddingLeft: 16,
      paddingTop: 8,
      paddingBottom: 24,
      fontSize: 16,
      ellipsizeMode: 'end',
    },
  });

  export default styles;