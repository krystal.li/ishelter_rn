/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */
import React, {useEffect, useState} from 'react';
import styles from '../styles/AnimalStyle';
import {fetchAnimals} from '../network/AnimalApi';
import {
  FlatList,
  View,
  Image,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';

const App = () => {
  interface State {
    isLoading: boolean;
    animals: any[];
  }   
  const [state, setState] = useState({isLoading: true, animals: []});

  useEffect(() => {
    fetchAnimals().then(response => {
      setState({isLoading: false, animals: response})
    })
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ActivityIndicator animating={state.isLoading} />
      <FlatList
        data={state.animals}
        renderItem={({item}) => (
          <View style={styles.container}>
            <Image source={{uri: item['album_file']}} style={styles.item} />
            <TouchableOpacity
              style={styles.favorite_container}
              // onPress={}
            >
              <Image
                style={styles.favorite}
                source={require('../../assets/ic_heart_white.png')}
              />
            </TouchableOpacity>
            <Text style={styles.title} numberOfLines={1}>
              {item['animal_kind']} {item['animal_place']}
            </Text>
          </View>
        )}
        keyExtractor={item => item['id']}
        numColumns={2}
      />
    </SafeAreaView>
  );
};

export default App;
