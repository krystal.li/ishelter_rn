export function fetchAnimals() {
  console.log('krystal fetchAnimals')
  return fetch(
    'https://data.coa.gov.tw/Service/OpenData/TransService.aspx?UnitId=QcbUEzN6E6DL&animal_status=OPEN&$top=30',
  )
    .then(response => response.json())
    .then(data => 
      data.filter((item: { [x: string]: string; }) => item['album_file'] != "")
    )
    .catch(error => console.error(error));
}